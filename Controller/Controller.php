<?php

/**
 * This File is part of the Selene\Components\Routing\Controller package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Controller;

use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpFoundation\JsonResponse;
use \Selene\Components\DI\ContainerAwareInterface;
use \Selene\Components\DI\Traits\ContainerAwareTrait;
use \Selene\Components\Kernel\Events\KernelEvents;
use \Selene\Components\Routing\Controller\EventAware;
use \Selene\Components\Routing\Events\RouteDispatchEvent;
use \Selene\Components\Routing\Controller\Controller as BaseController;

/**
 * @class Controller implements ContainerAwareInterface
 * @see ContainerAwareInterface
 *
 * @package Selene\Components\Routing\Controller
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
abstract class Controller extends BaseController implements ContainerAwareInterface, EventAware
{
    use ContainerAwareTrait;

    /**
     * routingEvent
     *
     * @var RouteDispatchEvent
     */
    private $routingEvent;

    /**
     * setEvent
     *
     * @param RouteDispatchEvent $event
     *
     * @return void
     */
    public function setEvent(RouteDispatchEvent $event)
    {
        $this->routingEvent = $event;
    }

    /**
     * getEvent
     *
     * @return RouteDispatchEvent
     */
    protected function getEvent()
    {
        return $this->routingEvent;
    }

    /**
     * ok
     *
     * @param mixed $content
     *
     * @return void
     */
    protected function ok($content = null)
    {
        $this->addResponseEvent($this->createResponse($content, 200));
    }

    /**
     * error
     *
     * @param mixed $content
     *
     * @return void
     */
    protected function error($content = null)
    {
        $this->addResponseEvent($this->createResponse($content, 500));
    }

    /**
     * notFound
     *
     * @param mixed $content
     *
     * @return void
     */
    protected function notFound($content = null)
    {
        $this->addResponseEvent($this->createResponse($content, 404));
    }

    /**
     * fireResponseEvent
     *
     * @param Response $response
     *
     * @return void
     */
    protected function addResponseEvent(Response $response)
    {
        if (($event = $this->getEvent()) && !$event->isPropagationStopped()) {
            $event->setResponse($response);
        }
    }

    /**
     * getEvents
     *
     * @return \Selene\Components\Events\Dispatcher
     */
    protected function getEvents()
    {
        return $this->getContainer()->get('app.events');
    }

    /**
     * getRouter
     *
     *
     * @return \Selene\Components\Routing\Router
     */
    protected function getRouter()
    {
        return $this->getContainer()->get('routing.router');
    }

    /**
     * createResponse
     *
     * @param mixed $content
     * @param int $status
     *
     * @return void
     */
    protected function createResponse($content = null, $status = 200)
    {
        if (is_array($content)) {
            $response = new JsonResponse($content);
        } else {
            $response = new Response($content);
        }

        $response->setStatusCode($status);

        return $response;
    }

    /**
     * redirect
     *
     * @param string $route
     * @param int    $status
     * @param array  $arguments
     * @param string $host
     *
     * @return Response
     */
    protected function redirect($route, $status = 302, $arguments = [], $host = null)
    {
        return $this->getContainer()->get('routing.redirect')->redirectRoute(
            $route,
            $status,
            $arguments,
            $host
        );
    }

    /**
     * render
     *
     * @param mixed $view
     *
     * @return Selene\Components\View\ViewManager
     */
    protected function render($view, array $context = [])
    {
        return $this->getContainer()->get('view')->render($view, $context);
    }

    /**
     * getRequest
     *
     * @return Symfony\Component\HttpFoundation\Request
     */
    protected function getRequest()
    {
        return $this->getContainer()->get('request.stack')->getCurrent();
    }

    /**
     * getView
     *
     * @return Selene\Components\View\ViewManager
     */
    protected function getView()
    {
        return $this->getContainer()->get('view');
    }
}
