<?php

/**
 * This File is part of the Selene\Packages\Framework package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Process;

use \Selene\Components\DI\Reference;
use \Selene\Components\DI\ContainerInterface;
use \Selene\Components\DI\Processor\ProcessInterface;
use \Selene\Components\DI\Definition\ServiceDefinition;
use \Selene\Components\DI\Definition\CallerDefinition;

/**
 * @class SetSessionStorage
 * @package Selene\Packages\Framework\Process
 * @version $Id$
 */
class TestProcess implements ProcessInterface
{
    /**
     * process
     *
     * @param ContainerInterface $container
     *
     * @return void
     */
    public function process(ContainerInterface $container)
    {
        $defA = new ServiceDefinition(__NAMESPACE__.'\\Foo');
        $defB = new ServiceDefinition(__NAMESPACE__.'\\Foo');
        $defC = new ServiceDefinition(__NAMESPACE__.'\\Foo');

        $container->setDefinition('foo', $defA);
        $container->setDefinition('bar', $defB);
        $container->setDefinition('baz', $defC);

        $defA->addSetter('setPath', [$caller = new CallerDefinition('bar', 'getValueFrom')]);

        $caller->setArguments([new Reference('baz')]);

        $defC->addSetter('setPath', [new Reference('bar')]);
    }
}

class Foo
{
    public function setPath()
    {

    }
}
