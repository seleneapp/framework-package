<?php

/**
 * This File is part of the Selene\Packages\Framework package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Subscriber;

use \Selene\Components\Events\SubscriberInterface;
use \Selene\Components\Events\Traits\SubscriberTrait;
use \Selene\Packages\Framework\View\Events\RenderEvent;
use \Selene\Packages\Framework\View\Events\ComposeEvent;
use \Selene\Packages\Framework\View\Events\ResolveEvent;
use \Selene\Packages\Framework\View\Events\RenderErrorEvent;
use \Selene\Packages\Framework\View\Events\ViewEvents as Events;

/**
 * @class ViewSubscriber
 * @package Selene\Packages\Framework
 * @version $Id$
 */
abstract class ViewSubscriber implements SubscriberInterface
{
    use SubscriberTrait;

    protected static $subscriptions = [
        Events::RENDERED       => 'onRendered',
        Events::RENDER_ERROR   => 'onRenderError',
        Events::COMPOSE        => 'onCompose',
        Events::ENGINE_RESOLVE => 'onEngineResolve',
    ];

    abstract public function onRendered(RenderEvent $event);

    abstract public function onRenderError(RenderErrorEvent $event);

    abstract public function onCompose(ComposeEvent $event);

    abstract public function onEngineResolve(ResolveEvent $event);
}
