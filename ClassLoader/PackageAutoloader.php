<?php

/**
 * This File is part of the Selene\Packages\Framework\ClassLoader package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\ClassLoader;

use \Selene\Components\Filesystem\Directory;
use \Selene\Components\Filesystem\Filesystem;

/**
 * @class PackageAutoloader
 * @package Selene\Packages\Framework\ClassLoader
 * @version $Id$
 */
class PackageAutoloader
{
    private $path;

    /**
     * __construct
     *
     * @param mixed $path
     *
     * @access public
     * @return void
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * load
     *
     * @access public
     * @return void
     */
    public function load()
    {
        $this->loadFromPath($this->path, 4);
    }

    /**
     * loadFromPath
     *
     * @param mixed $path
     *
     * @access public
     * @return void
     */
    public function loadFromPath($path, $depth = 0)
    {
        if (0 === $depth) {
            return false;
        }

        if (!is_dir($path)) {
            return;
        }


        foreach (new \FilesystemIterator($path, \FilesystemIterator::SKIP_DOTS) as $fpath => $finfo) {

            if (0 === $depth) {
                break;
            }

            if ($finfo->isDir()) {
                if (false === $this->loadFromPath($fpath, $depth)) {
                    break;
                }
                continue;
            }

            if ($finfo->isFile() && 'autoload.php' === $finfo->getBaseName()) {
                include_once $finfo->getRealPath();
            }
        }

        $depth--;
    }
}
