<?php

/**
 * This File is part of the Selene\Packages\Middleware package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Middleware;

use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * @class StubKernel implements HttpKernelInterface
 * @see HttpKernelInterface
 *
 * @package Selene\Packages\Middleware
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class FooKernel extends StubKernel
{
    /**
     * priority
     *
     * @var int
     */
    protected $priority = 20;

    /**
     * handle
     *
     * @param Request $request
     * @param mixed $type
     * @param mixed $catch
     *
     * @access public
     * @return mixed
     */
    public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = true)
    {
        var_dump('me too');
        return $this->getKernel()->handle($request, $type, $catch);
    }
}
