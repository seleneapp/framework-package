<?php

/**
 * This File is part of the Selene\Packages\Framework\View package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\View;

use \Selene\Components\Events\Dispatcher;
use \Selene\Components\View\ViewManager;
use \Selene\Components\View\Exception\RenderException;
use \Selene\Components\View\Composer\ComposerInterface;
use \Selene\Packages\Framework\View\Events\RenderEvent;
use \Selene\Packages\Framework\View\Events\RenderErrorEvent;
use \Selene\Packages\Framework\View\Events\ComposeEvent;
use \Selene\Packages\Framework\View\Events\ResolveEvent;
use \Selene\Packages\Framework\View\Events\ViewEvents as Events;

/**
 * @class EventedView
 * @package Selene\Packages\Framework\View
 * @version $Id$
 */
class EventedView extends ViewManager
{
    /**
     * Constructor.
     * @param Dispatcher $events
     * @param array $engines
     * @param ComposerInterface $composer
     */
    public function __construct(Dispatcher $events, array $engines = [], ComposerInterface $composer = null)
    {
        $this->events = $events;
        parent::__construct($engines, $composer);
    }

    /**
     * {@inheritdoc}
     */
    public function findEngine($template)
    {
        $engine = parent::findEngine($template);
        $this->events->dispatch(Events::ENGINE_RESOLVE, new ResolveEvent($engine));

        return $engine;
    }

    /**
     * {@inheritdoc}
     */
    protected function doRender($template, $context)
    {
        try {
            parent::doRender($template, $context);
        } catch (RenderException $e) {
            $this->events->dispatch(Events::RENDER_ERROR, new RenderErrorEvent($e, $template));

            throw $e;
        }

        $this->events->dispatch(Events::RENDERED, new RenderEvent($template));
    }

    /**
     * {@inheritdoc}
     */
    protected function callComposer($template, $context)
    {
        parent::callComposer($template, $context);

        $this->events->dispatch(Events::COMPOSE, new ComposeEvent($template));
    }
}
