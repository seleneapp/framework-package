<?php

/**
 * This File is part of the \Users\malcolm\www\selene_source\src\Selene\Packages\Framework\Console package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Console;

use \Selene\Components\Console\Command;
use \Selene\Components\Filesystem\Traits\PathHelperTrait;
use \Symfony\Component\Console\Application;
use \Symfony\Component\Console\Input\ArrayInput;
use \Symfony\Component\Console\Output\NullOutput;
use \Symfony\Component\Console\Input\InputOption;
use \Symfony\Component\Console\Output\OutputInterface;
use \Symfony\Component\Console\Input\InputInterface;

/**
 * @class ContainerClearCache
 * @package \Users\malcolm\www\selene_source\src\Selene\Packages\Framework\Console
 * @version $Id$
 */
class ContainerClearCache extends Command
{
    use PathHelperTrait;

    protected $name = 'container:clearcache';

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Clear the container cache';
    }

    /**
     * execute
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @access protected
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $app = $this->getApp();

        $env = $app->getEnvironment();

        $container = $app->getContainer();
        $fs = $container->get('filesystem');

        //var_dump($this->substitutePaths('/foo', '/baz/bar'));
        //TODO: check if the cache path is within the cached container path.
        //prevent deleting the actual container class from the framework.
        try {

            $name = $this->getContainerFileName($env);

            $containerRel = $this->substitutePaths(
                $app->getContainerCachePath(),
                $containerPath = $this->getContainerFilePath($container, $name)
            );

            if ($containerRel !== basename($containerPath)) {
                throw new \Exception(sprintf('container "%s" does not match.', $containerRel));
            }

            $this->substitutePaths(
                $app->getContainerCachePath(),
                $metaPath = $this->getManifestFilePath($container, $name)
            );

            $fs->remove($containerPath);
            $fs->remove($metaPath);

        } catch (\Exception $e) {
            $output->writeln('<error>'.$e->getMessage().'</error>');
            return;
        }

        $output->writeln('<info>container cache has been cleared</info>');
    }

    /**
     * getContainerFileName
     *
     * @param mixed $env
     *
     * @access private
     * @return mixed
     */
    private function getContainerFileName($env)
    {
        return 'Container' . ucfirst(strtolower($env));
    }

    /**
     * getContainerFilePath
     *
     * @param mixed $container
     * @param mixed $name
     *
     * @access private
     * @return string
     */
    private function getContainerFilePath($container, $name)
    {
        $reflection = new \ReflectionObject($container);

        return $reflection->getFileName();
    }

    /**
     * getManifestFilePath
     *
     * @param mixed $container
     * @param mixed $name
     *
     * @access private
     * @return string
     */
    private function getManifestFilePath($container, $name)
    {
        $path = $this->getContainerFilePath($container, $name);

        return dirname($path).DIRECTORY_SEPARATOR.basename($path, 'php').'manifest';
    }
}
