<?php

/**
 * This File is part of the Selene\Packages\Framework\Process package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Process;

use \Selene\Components\DI\Reference;
use \Selene\Components\DI\Meta\MetaDataInterface;
use \Selene\Components\DI\ContainerInterface;
use \Selene\Components\DI\Processor\ProcessInterface;
use \Selene\Components\DI\Definition\DefinitionInterface;

/**
 * @class RegisterListener
 * @package Selene\Packages\Framework\Process
 * @version $Id$
 */
abstract class RegisterListener implements ProcessInterface
{
    /**
     * container
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     * reflections
     *
     * @var array
     */
    private $reflections;


    public function process(ContainerInterface $container)
    {
        $this->reflections = [];

        $this->container = $container;
    }

    /**
     * addViewListener
     *
     * @param mixed $id
     * @param MetaDataInterface $tag
     *
     * @return void
     */
    protected function addListener($id, MetaDataInterface $tag)
    {
        $events = $this->container->getDefinition('app.events');

        if ($this->listenerIsListener($id, $tag, $events)) {
            return;
        } elseif ($this->listenerIsSubscriber($id)) {
            $events->addSetter('addSubscriber', [new Reference($id)]);
        } else {
            throw new \LogicException(
                sprintf('Cannot set service "%s" as event listener, no handler was found.', $id)
            );
        }
    }

    /**
     * listenerIsListener
     *
     * @param string $id
     * @param MetaDataInterface $tag
     * @param DefinitionInterface $dispatcher
     *
     * @return boolean
     */
    protected function listenerIsListener($id, MetaDataInterface $tag, DefinitionInterface $dispatcher)
    {
        $ref = $this->getListenerReflection($id);
        $isListener = $ref->implementsInterface('Selene\Components\Events\EventListenerInterface');

        var_dump($tag);
        if (!$events = $tag->get('event_name')) {
            return false;
        }

        $callbacks = (array)$tag->get('callback');

        foreach ((array)$events as $i => $event) {

            if ($isListener) {
                $dispatcher->addSetter('addListener', [$event, new Reference($id)]);
                continue;
            }

            if (!isset($callbacks[$i])) {
                throw new \InvalidArgumentException(
                    sprintf('No event callback found for service "%s".', $id)
                );
            }

            $dispatcher->addSetter('on', $this->getListenerCallback($id, $event, $callbacks[$i]));
        }

        return true;
    }

    /**
     * getListenerCallback
     *
     * @param string $id
     * @param string $event
     * @param string $callback
     *
     * @return array
     */
    protected function getListenerCallback($id, $event, $callback = null)
    {
        try {
            $method = $this->getListenerReflection($id)->getMethod($callback);
        } catch (\ReflectionException $e) {
            throw new \InvalidArgumentException(
                sprintf('Cannot set service %s as event listener, method %s does not exist.', $id, $callback)
            );
        }

        if (!$method->isPublic()) {
            throw new \InvalidArgumentException(
                sprintf('Cannot set service %s as event listener, method %s is not public.', $id, $callback)
            );
        }

        var_dump([$event, [new Reference($id), $callback]]);

        return [$event, [new Reference($id), $callback]];
    }

    /**
     * listenerIsSubscriber
     *
     * @param string $id
     *
     * @return boolean
     */
    protected function listenerIsSubscriber($id)
    {
        $ref = $this->getListenerReflection($id);

        return $ref->implementsInterface('Selene\Components\Events\SubscriberInterface');
    }

    /**
     * getListenerReflection
     *
     * @param string $id
     *
     * @return \ReflectionClass
     */
    protected function getListenerReflection($id)
    {
        if (!isset($this->reflections[$id])) {
            $service = $this->container->get($id);
            $class = $this->container->getParameters()->resolveParam($service);

            $this->reflections[$id] = new \ReflectionClass($class);
        }

        return $this->reflections[$id];
    }
}
