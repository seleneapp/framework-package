<?php

/**
 * This File is part of the Selene\Packages\Framework\View\Events package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\View\Events;

use \Selene\Components\View\Exception\RenderException;

/**
 * @class RenderEvent
 * @package Selene\Packages\Framework\View\Events
 * @version $Id$
 */
class RenderErrorEvent extends RenderEvent
{
    private $exception;

    /**
     * Constructor.
     *
     * @param RenderException $e
     * @param mixed $tempalte
     *
     */
    public function __construct(RenderException $e, $template = null)
    {
        $this->exception = $e;
        parent::__construct($template);
    }

    /**
     * Get the render exception object.
     *
     * @return RenderException
     */
    public function getException()
    {
        return $this->exception;
    }
}
