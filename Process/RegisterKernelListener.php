<?php

/**
 * This File is part of the Selene\Packages\Framework\Process package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Process;

use \Selene\Components\DI\Reference;
use \Selene\Components\DI\Meta\MetaDataInterface;
use \Selene\Components\DI\ContainerInterface;
use \Selene\Components\DI\Processor\ProcessInterface;

/**
 * @class RegisterApplicationListeners
 * @package Selene\Packages\Framework\Process
 * @version $Id$
 */
class RegisterKernelListener extends RegisterListener
{

    /**
     * Attach event listeners to application eventns.
     *
     * @param ContainerInterface $container
     *
     * @access public
     * @return void
     */
    public function process(ContainerInterface $container)
    {
        parent::process($container);

        if (!$container->hasDefinition('app.kernel')) {
            return;
        }

        foreach ($container->findDefinitionsWithMetaData($tag = 'app.events') as $id => $definition) {
            $this->addListener($id, $definition->getMetaData($tag));
        }
    }

    /**
     * registerMiddleware
     *
     * @param string $id
     *
     * @access private
     * @return void
     */
    private function addKernelListener($id, MetaDataInterface $tag)
    {
        $eventDefinition = $this->container->getDefinition('app_events');

        foreach ((array)$tag->get('event_name') as $eventInfo) {
            var_dump($eventInfo);
        }

        //if (null === ($event = $tag->get('event_name')) || null === ($eventHandler = $tag->get('event_handler'))) {
        //    throw new \InvalidArgumentException();
        //}

        //$eventDefinition->addSetter('on', [$event, [new Reference($id), $eventHandler]]);
    }
}
