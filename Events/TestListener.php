<?php

/**
 * This File is part of the Selene\Packages\Framework package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Events;

/**
 * @class TestListener
 * @package Selene\Packages\Framework
 * @version $Id$
 */
class TestListener
{
    public function onKernelHandleRequest()
    {
        var_dump(func_get_args());
    }

    public function onKernelException()
    {
        var_dump(func_get_args());
    }
}
