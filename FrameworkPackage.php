<?php

/**
 * This File is part of the Selene\Package\Framework package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework;

use \Selene\Components\Package\Package;
use \Selene\Components\Package\ExportResourceInterface;
use \Selene\Components\Package\FileRepositoryInterface;
use \Selene\Components\Console\Application as Console;
use \Selene\Components\DI\ContainerInterface;
use \Selene\Components\DI\Processor\ProcessorInterface;
use \Selene\Components\DI\BuilderInterface as ContainerBuilderInterface;
use \Selene\Packages\Framework\Middleware\FooKernel;
use \Selene\Packages\Framework\Middleware\StubKernel;
use \Selene\Packages\Framework\Middleware\BarKernel;
use \Selene\Packages\Framework\Console\Commands\RoutesCommand;
use \Selene\Packages\Framework\Console\Commands\ServicesCommand;
use \Selene\Packages\Framework\Console\ContainerClearCache;
use \Selene\Packages\Framework\Console\PackageListCommand;
use \Selene\Packages\Framework\Console\PackageGenerateCommand;
use \Selene\Packages\Framework\Console\PackagePublishCommand;
use \Selene\Packages\Framework\Console\AppEnvironmentCommand;
use \Selene\Packages\Framework\Process\AddRouteCache;
use \Selene\Packages\Framework\Process\RegisterMiddleware;
use \Selene\Packages\Framework\Process\RegisterViewListener;
use \Selene\Packages\Framework\Process\RegisterKernelListener;
use \Selene\Packages\Framework\Process\RegisterViewComposers;
use \Selene\Packages\Framework\Process\RegisterTemplatePaths;
use \Selene\Packages\Framework\Process\ReplaceViewManger;
use \Selene\Packages\Framework\Process\ReplaceLogger;
use \Selene\Packages\Framework\Process\TestProcess;

/**
 * @class FrameworkPackage extends Package
 * @see Package
 *
 * @package Selene\Packages\Framework
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class FrameworkPackage extends Package implements ExportResourceInterface
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilderInterface $builder)
    {
        $builder->getProcessor()
            ->add(new AddRouteCache, ProcessorInterface::BEFORE_OPTIMIZE)
            ->add(new RegisterMiddleware, ProcessorInterface::OPTIMIZE)
            ->add(new RegisterKernelListener, ProcessorInterface::BEFORE_OPTIMIZE)
            ->add(new RegisterViewListener, ProcessorInterface::BEFORE_OPTIMIZE)
            ->add(new RegisterViewComposers, ProcessorInterface::BEFORE_OPTIMIZE)
            ->add(new RegisterTemplatePaths, ProcessorInterface::OPTIMIZE)
            ->add(new ReplaceViewManger, ProcessorInterface::AFTER_REMOVE)
            ->add(new ReplaceLogger, ProcessorInterface::BEFORE_OPTIMIZE);
    }

    /**
     * {@inheritdoc}
     */
    public function registerCommands(Console $console)
    {
        $container = $console->getApplication()->getContainer();

        $console->add($container->get('command.routes:list'));
        $console->add($container->get('command.package:publish'));
        $console->add(new ContainerClearCache);
        $console->add(new AppEnvironmentCommand);
    }

    public function getExports(FileRepositoryInterface $files)
    {
        $files->createTarget($this->getResourcePath().'/publish/config_test.xml');
        $files->createTarget($this->getResourcePath().'/publish/foo.xml', '/../../../config');
    }
}
