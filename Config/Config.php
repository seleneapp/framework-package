<?php

/**
 * This File is part of the Selene\Packages\Framework\Config package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Config;

use \Selene\Components\Package\PackageConfiguration;
use \Selene\Components\DI\BuilderInterface;
use \Selene\Components\DI\ContainerInterface;
use \Selene\Components\DI\Reference;
use \Selene\Components\DI\Loader\XmlLoader;
use \Selene\Components\Config\Resource\Locator;
//use \Selene\Components\Routing\Loader\XmlLoader as RoutingLoader;
use \Selene\Components\Config\Resource\DelegatingLoader;

use \Selene\Components\Routing\Loader\PhpLoader as PhpRoutingLoader;
use \Selene\Components\Routing\Loader\XmlLoader as XmlRoutingLoader;
use \Selene\Components\Routing\Loader\CallableLoader as CallableRoutingLoader;

/**
 * @class Config extends Configuration
 * @see Configuration
 *
 * @package Selene\Packages\Framework\Config
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class Config extends PackageConfiguration
{
    /**
     * load
     *
     * @param ContainerInterface $container
     * @param array $values
     *
     * @access public
     * @return void
     */
    public function setup(BuilderInterface $builder, array $values)
    {
        $container = $builder->getContainer();

        $loader = $this->getConfigLoader($builder, $locator = new Locator([$this->getResourcePath()]));

        $loader->load('services.xml');

        $config = $this->mergeValues($values);

        $this->setUpLogging($container, $config);

        $container->getParameters()->set('view.events', $config['view']['events']);

        $container->getParameters()->set('cache_location', $config['cache']['location']);
        $container->getParameters()->set('cache_driver', $config['cache']['driver']);

        if (isset($config['controller']['namespaces'])) {
            $container->getParameters()->set('app.controller_namespaces', $config['controller']['namespaces']);
        }

        $const = array_pad(explode('::', $config['database']['pdo']), -1, null);

        $pdo = new \ReflectionClass('PDO');

        //var_dump($pdo->hasConstant($const[1]));


        // ----------------------------------------------------------------------------------
        // ROUTING:
        // ----------------------------------------------------------------------------------
        // Route cache storage
        //
        //

        $this->setUpRoutingCacheDriver($container, $config);

        $loader = $this->getRoutingLoader($builder, $container->get('routing.routes'), $locator);

        $userFile;

        // User defined routes
        if (isset($config['routing']['resource'])) {
            $file = $container->getParameters()->resolveParam($config['routing']['resource']);

            $locator->addPath(dirname($file));
            $userFile = basename($file);
        }

        $loader->load($routing = 'routing.xml');

        if (null !== $userFile && $routing !== $userFile) {
            $loader->load($userFile);
        }

        return [];
    }

    /**
     * setUpRoutingCacheDriver
     *
     * @param ContainerInterface $container
     * @param array $config
     *
     * @access protected
     * @return void
     */
    protected function setUpRoutingCacheDriver(ContainerInterface $container, array $config)
    {
        $driver = $this->getDefaultArray($config, 'routing.cache.driver', 'filesystem');

        if ('filesystem' === $driver) {

            $path = $this->getDefaultArray(
                $config,
                'routing.cache.driver.filesystem.location',
                $container->getParameter('routing.cache_path')
            );

            $this->registerRoutingFilesystemCache($container, $path);

        } elseif ('memcached' === $driver) {

            $servers = $this->getDefaultArray(
                $config,
                'routing.cache.driver.memcached.servers',
                [$container->getParameter('routing.cache_memcached.servers')]
            );

            $this->registerRoutingMemcachedCache($container, $servers);
        }
    }

    /**
     * registerRoutingFilesystemCache
     *
     * @param ContainerInterface $container
     * @param mixed $path
     *
     * @access protected
     * @return void
     */
    protected function registerRoutingFilesystemCache(ContainerInterface $container, $path)
    {
        $container->setParameter('routing.cache_path', $path);
        $container->setParameter('routing.cache_driver.class', 'Selene\Components\Routing\Cache\FilesystemDriver');

        $container->define('routing.cache_driver', '%routing.cache_driver.class%')
            ->addArgument('%routing.cache_path%');
    }

    /**
     * registerRoutingMemcachedCache
     *
     * @param ContainerInterface $container
     * @param array $servers
     *
     * @access protected
     * @return void
     */
    protected function registerRoutingMemcachedCache(ContainerInterface $container, array $servers)
    {
        $container->setParameter('routing.cache_memcached.servers', $servers);
        $container->setParameter('routing.cache_driver.class', 'Selene\Components\Routing\Cache\MemcachedDriver');

        $container->setParameter(
            'routing.cache_memcached.connection.class',
            'Selene\Components\Cache\Driver\MemcachedConnection'
        );

        $container->define('routing.cache_memcached.instance', 'Memcached')
            ->setInternal(true);

        $container->define('routing.cache_memcached.connection', '%routing.cache_memcached.connection.class%')
            ->addArgument(new Reference('routing.cache_memcached.instance'))
            ->addArgument('%routing.cache_memcached.servers%')
            ->setInternal(true);

        $container->define('routing.cache_driver', '%routing.cache_driver.class%')
            ->addArgument(new Reference('routing.cache_memcached.connection'));
    }

    protected function setUpLogging(ContainerInterface $container, array $config)
    {
        $loggingService = $this->getDefaultArray(
            $config,
            'logging.logger',
            $this->getParameter('logger.default', 'logger')
        );

        $this->setParameter('logger.default', $loggingService);
    }

    /**
     * getConfigTree
     *
     * @access public
     * @return NodeInterface
     */
    public function getConfigTree()
    {
        $root = $this->getConfigBuilder()->getRoot();

        $root
            ->boolean('is_resource_file')
            ->optional()
            ->defaultValue(true)
            ->end()
            ->dict('database')
                ->string('default')
                ->end()
                ->dict('connections')
                //->atLeastOne('default')
                ->end()
            ->end()
            ->dict('cache')
                ->enum('driver')
                ->values('memcache', 'file', 'apc', 'memory')
                ->end()
                ->string('location')
                ->end()
            ->end();

        return $root;
    }
}
