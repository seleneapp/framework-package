<?php

/**
 * This File is part of the Selene\Packages\Framework\Process package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Process;

use \Selene\Components\DI\Reference;
use \Selene\Components\DI\Meta\MetaDataInterface;
use \Selene\Components\DI\ContainerInterface;
use \Selene\Components\DI\Processor\ProcessInterface;

/**
 * @class RegisterApplicationListeners
 * @package Selene\Packages\Framework\Process
 * @version $Id$
 */
class RegisterViewComposers implements ProcessInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Attach event listeners to application eventns.
     *
     * @param ContainerInterface $container
     *
     * @access public
     * @return void
     */
    public function process(ContainerInterface $container)
    {
        $this->container = $container;

        if (!$container->hasDefinition('view.composer')) {
            return;
        }

        $composer = $container->getDefinition('view.composer');

        foreach ($container->findDefinitionsWithMetaData($tag = 'view_compose') as $id => $definition) {
            foreach ((array)$definition->getMetaData($tag)->get('template') as $template) {
                $composer->addSetter('compose', [$template, new Reference($id)]);
            }
        }
    }
}
