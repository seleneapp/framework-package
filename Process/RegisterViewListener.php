<?php

/**
 * This File is part of the Selene\Packages\Framework\Process package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Process;

use \Selene\Components\DI\Reference;
use \Selene\Components\DI\ContainerInterface;
use \Selene\Components\DI\Meta\MetaDataInterface;
use \Selene\Components\DI\Processor\ProcessInterface;
use \Selene\Components\DI\Definition\DefinitionInterface;

/**
 * @class ReplaceViewManger
 * @package Selene\Packages\Framework\Process
 * @version $Id$
 */
class RegisterViewListener extends RegisterListener
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerInterface $container)
    {
        parent::process($container);

        if (!$container->hasDefinition('view')) {
            return;
        }

        if (!$container->getParameters()->has('view.events') || !$container->getParameters()->get('view.events')) {
            return;
        }

        foreach ($container->findDefinitionsWithMetaData($tag = 'view.events') as $id => $definition) {
            $this->addListener($id, $definition->getMetaData($tag));
        }
    }
}
