<?php

/**
 * This File is part of the Selene\Packages\Framework package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Process;

use \Selene\Components\DI\ContainerInterface;
use \Selene\Components\DI\Processor\ProcessInterface;

/**
 * @class ReplaceViewManger
 * @package Selene\Packages\Framework
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class ReplaceLogger implements ProcessInterface
{
    /**
     * Replace the default null logger.
     *
     * {@inheritddoc}
     */
    public function process(ContainerInterface $container)
    {
        $params = $container->getParameters();

        if (!$container->hasDefinition('logger')) {
            throw new \RuntimeException('No logger service found');
        }

        // if no default logger service id is defined, throw
        if (!$params->has('logger.default') ||
            !$container->hasDefinition($id = $params->resolveParam($params->get('logger.default')))
        ) {
            throw new \RuntimeException('No default logger service defined');
        }

        if ('logger' === $id) {
            return;
        }

        // remove the actual definition:
        $container->removeDefinition('logger');
        // set the definition alias to 'logger':
        $container->setAlias('logger', $id);
    }
}
