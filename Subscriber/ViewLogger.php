<?php

/**
 * This File is part of the Selene\Packages\Framework package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Subscriber;

use \Psr\Log\LoggerInterface;
use \Selene\Components\Events\SubscriberInterface;
use \Selene\Packages\Framework\View\Events\RenderEvent;
use \Selene\Packages\Framework\View\Events\ComposeEvent;
use \Selene\Packages\Framework\View\Events\ResolveEvent;
use \Selene\Packages\Framework\View\Events\RenderErrorEvent;
use \Selene\Packages\Framework\View\Events\ViewEvents as Events;

/**
 * @class ViewLogger
 * @package Selene\Packages\Framework
 * @version $Id$
 */
class ViewLogger implements SubscriberInterface
{
    private $debug;
    private $logger;

    protected static $subscriptions = [
        Events::RENDERED       => 'onRendered',
        Events::RENDER_ERROR   => 'onRenderError',
        Events::COMPOSE        => 'onCompose',
        Events::ENGINE_RESOLVE => 'onEngineResolve',
    ];

    public function __construct(LoggerInterface $logger, $debug = true)
    {
        $this->logger = $logger;
        $this->debug = (bool)$debug;
    }

    public function getSubscriptions()
    {
        if (false !== $this->debug) {
            return static::$subscriptions;
        }

        return [Events::RENDER_ERROR => static::$subscriptions[Events::RENDER_ERROR]];
    }

    /**
     * onRendered
     *
     * @param RenderEvent $event
     *
     * @return void
     */
    public function onRendered(RenderEvent $event)
    {
        $this->logger->debug('[RENDERED]:', ['template' => $event->getTemplate()]);
    }

    /**
     * onRenderError
     *
     * @param RenderErrorEvent $event
     *
     * @return void
     */
    public function onRenderError(RenderErrorEvent $event)
    {
        $this->logger->critical('[RENDER_ERROR]:', ['error' => $event->getException()->getMessage()]);
    }

    /**
     * onCompose
     *
     * @param ComposeEvent $event
     *
     * @return void
     */
    public function onCompose(ComposeEvent $event)
    {
        $this->logger->debug('[RENDER_COMPOSE]:', ['template' => $event->getTemplate()]);
    }

    /**
     * onEngineResolve
     *
     * @param ResolveEvent $event
     *
     * @return void
     */
    public function onEngineResolve(ResolveEvent $event)
    {
        $this->logger->debug('[RENDER_ENGINE]:', ['engine' => get_class($event->getEngine())]);
    }
}
