<?php

/**
 * This File is part of the Selene\Packages\Framework\Console\Commands package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Console\Commands;

use \Selene\Components\Console\Command;
use \Symfony\Component\Console\Helper\TableHelper;
use \Symfony\Component\Console\Application;
use \Symfony\Component\Console\Input\ArrayInput;
use \Symfony\Component\Console\Output\NullOutput;
use \Symfony\Component\Console\Input\InputOption;
use \Symfony\Component\Console\Output\OutputInterface;
use \Symfony\Component\Console\Input\InputInterface;
use \Selene\Components\Routing\RouteCollectionInterface;

/**
 * @class RoutesCommand
 * @package Selene\Packages\Framework\Console\Commands
 * @version $Id$
 */
class RoutesListCommand extends Command
{
    /**
     * name
     *
     * @var string
     */
    protected $name = 'routes:list';

    /**
     * description
     *
     * @var string
     */
    protected $description = 'List all application routes';

    /**
     * routes
     *
     * @var RouteCollectionInterface
     */
    private $routes;

    /**
     * @param RouteCollectionInterface $routes
     */
    public function __construct(RouteCollectionInterface $routes)
    {
        $this->routes = $routes;

        parent::__construct($this->getName());
    }

    /**
     * execute
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function fire()
    {
        $this->createTable($this->getTableHeader(), $this->getTableRows(), TableHelper::LAYOUT_BORDERLESS)
            ->render($this->getOutput());
    }

    /**
     * getTableHeader
     *
     *
     * @access protected
     * @return array
     */
    protected function getTableHeader()
    {
        return ['pattern', 'method', 'name', 'host', 'secure', 'controller', 'before', 'after'];
    }

    /**
     * getTableRows
     *
     * @access protected
     * @return array
     */
    protected function getTableRows()
    {
        $routes = $this->getRoutes();

        $rows = [];

        foreach ($routes as $name => $route) {
            $row =   [$route->getPattern(), implode('|', $route->getMethods()), $name, $route->getHost() ?: '-'];
            $row[] = $route->isSecure() ? 'Yes' : '-';
            $row[] = $route->getAction();

            $row[] =  implode('|', $route->getBeforeFilters()) ?: '-';
            $row[] =  implode('|', $route->getAfterFilters()) ?: '-';

            $rows[] = $row;
        }

        return $rows;
    }

    /**
     * formatRoutesDisplay
     *
     * @access protected
     * @return string
     */
    protected function formatRoutesDisplay()
    {
        return 'display routes';
    }

    /**
     * getRoutes
     *
     * @access protected
     * @return void
     */
    protected function getRoutes()
    {
        return $this->routes;
    }
}
