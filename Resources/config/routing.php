<?php

/**
 * This File is part of the Selene\Packages\Framework package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

/*

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
ROUTING
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

*/

$routes->get('index', '/', ['_action' => 'framework:demo:index']);
    //->setHost('selene.{tld}')
    //->setHostConstraint('tld', 'com|dev');

$routes->get('welcome', '/welcome/{name?}', ['_action' => 'framework:demo:welcome']);
$routes->get('test', '/test/{name?}', ['_action' => 'demo_controller:welcomeAction']);

$routes->resource('photo', 'framework:photo');
