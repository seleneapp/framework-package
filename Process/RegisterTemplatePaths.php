<?php

/**
 * This File is part of the Selene\Packages\Framework\Process package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Process;

use \Selene\Components\DI\Reference;
use \Selene\Components\DI\Meta\MetaDataInterface;
use \Selene\Components\DI\ContainerInterface;
use \Selene\Components\DI\Processor\ProcessInterface;

/**
 * @class RegisterApplicationListeners
 * @package Selene\Packages\Framework\Process
 * @version $Id$
 */
class RegisterTemplatePaths implements ProcessInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Attach event listeners to application eventns.
     *
     * @param ContainerInterface $container
     *
     * @access public
     * @return void
     */
    public function process(ContainerInterface $container)
    {
        $parameters = $container->getParameters();
        $locator = $container->getDefinition('view.template_locator');

        $templates = (array)$parameters->get('template_paths');

        foreach ($parameters->get('app.package_paths') as $namespace => $path) {

            if (!is_dir($path = $path . '/Resources/view')) {
                continue;
            }

            $templates[$namespace] = $path;
        }

        $locator->replaceArgument($templates, 0);
    }
}
