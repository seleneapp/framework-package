<?php

/**
 * This File is part of the Selene\Packages\Framework package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Process;

use \Selene\Components\DI\Reference;
use \Selene\Components\DI\ContainerInterface;
use \Selene\Components\DI\Processor\ProcessInterface;
use \Selene\Components\DI\Definition\ServiceDefinition;

/**
 * @class AddRouteCache
 * @package Selene\Packages\Framework
 * @version $Id$
 */
class AddRouteCache implements ProcessInterface
{
    /**
     * process
     *
     * @param ContainerInterface $container
     *
     * @access public
     * @return void
     */
    public function process(ContainerInterface $container)
    {
        $container->removeDefinition('routes');
        $container->getDefinition('routing.cache')->setInternal(false);
        $container->get('routing.cache')->write($container->get('routing.routes'));
        $container->getDefinition('routing.cache')->setInternal(true);

        $routesDefinition = new ServiceDefinition('\Selene\Components\Routing\Cache\CachedRoutes');
        $routesDefinition->setArguments([new Reference('routing.cache')]);

        $container->setDefinition('routing.routes', $routesDefinition);
    }
}
