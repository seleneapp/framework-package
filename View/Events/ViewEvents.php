<?php

/**
 * This File is part of the Selene\Packages\Framework\View\Events package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\View\Events;

/**
 * @class ViewEvents
 * @package Selene\Packages\Framework\View\Events
 * @version $Id$
 */
class ViewEvents
{
    const BEFORE_RENDER  = 'view.before_render';

    const RENDER_ERROR   = 'view.render_error';

    const RENDERED       = 'view.rendered';

    const ENGINE_RESOLVE = 'view.engin_resolve';

    const COMPOSE        = 'view.compose';

    private function __construct()
    {
    }
}
