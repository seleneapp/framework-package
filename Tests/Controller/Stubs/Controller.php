<?php

/**
 * This File is part of the Selene\Packages\Framework package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Tests\Controller\Stubs;

use \Selene\Packages\Framework\Controller\Controller as BaseController;

/**
 * @class Controller
 * @package Selene\Packages\Framework
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class Controller extends BaseController
{
    public function actionIndex($str)
    {
        return $this->render($str);
    }

    public function createUser()
    {
        $id = $this->getRequest()->request->get('user_id');
        return $id;
    }

    public function testAction()
    {
        return 'ok';
    }
}
