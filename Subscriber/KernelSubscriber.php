<?php

/**
 * This File is part of the Selene\Packages\Framework package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Subscriber;

use \Selene\Components\Events\EventInterface;
use \Selene\Components\Events\SubscriberInterface;
use \Selene\Components\Events\Traits\SubscriberTrait;
use \Selene\Components\Kernel\Events\KernelEvents as Events;
use \Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * @class ViewSubscriber
 * @package Selene\Packages\Framework
 * @version $Id$
 */
class KernelSubscriber implements SubscriberInterface
{
    use SubscriberTrait;

    protected static $subscriptions = [
        Events::REQUEST => 'onRequest'
    ];

    public function onRequest(EventInterface $event)
    {
        //var_dump('[REQUEST_TYPE] ' .$this->getRequestTypeString($event->getRequestType()));
    }

    /**
     * getRequestTypeString
     *
     * @param int $type
     *
     * @return string
     */
    private function getRequestTypeString($type)
    {
        if (HttpKernelInterface::MASTER_REQUEST === $type) {
            return 'master request';
        }

        if (HttpKernelInterface::SUB_REQUEST === $type) {
            return 'sub request';
        }

        return 'unkown';
    }
}
