<?php

/**
 * This File is part of the Selene\Packages\Framework\Process package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Process;

use \Selene\Components\DI\Reference;
use \Selene\Components\DI\ContainerInterface;
use \Selene\Components\DI\Processor\ProcessInterface;

/**
 * @class RegisterMiddleware
 * @package Selene\Packages\Framework\Process
 * @version $Id$
 */
class RegisterMiddleware implements ProcessInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * process
     *
     * @param ContainerInterface $container
     *
     * @access public
     * @return mixed
     */
    public function process(ContainerInterface $container)
    {
        $this->container = $container;

        //foreach ($container->getDefinitionMetaData() as $id => $definition) {
        //    if ($definition->hasMetaData('middleware')) {
        //        $this->registerMiddleware($id);
        //    }
        //}
    }

    /**
     * registerMiddleware
     *
     * @param string $id
     *
     * @access private
     * @return void
     */
    private function registerMiddleware($id)
    {
        $reflection = new \ReflectionClass($this->container->getDefinition($id)->getClass());

        if (!$reflection->implementsInterface($interface = '\Selene\Components\Stack\StackedKernelInterface')) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Service %s tagged as middleware must implement %s',
                    $id,
                    $interface
                )
             );
        }

        $builder = $this->container->getDefinition('kernel_stackbuilder');
        $builder->addSetter('add', [new Reference($id)]);
    }
}
