<?php

/**
 * This File is part of the Selene\Packages\Framework package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Console\Commands;

use \Selene\Components\Console\Command;
use \Selene\Components\Events\SubscriberInterface;
use \Selene\Components\Package\PackagePublisher;
use \Selene\Components\Package\Events\PublishEvents;
use \Selene\Components\Package\Events\PackageEvent;
use \Selene\Components\Package\Events\PackagePublishEvent;
use \Selene\Components\Package\Events\PackageExceptionEvent;
use \Symfony\Component\Console\Input\InputOption;
use \Symfony\Component\Console\Input\InputArgument;

/**
 * @class PackagePublishCommand
 * @package Selene\Components\Package
 * @version $Id$
 */
class PackagePublishCommand extends Command implements SubscriberInterface
{
    /**
     * name
     *
     * @var string
     */
    protected $name = 'package:publish';

    /**
     * @param PackagePublisher $publisher
     */
    public function __construct(PackagePublisher $publisher)
    {
        $this->publisher = $publisher;

        parent::__construct($this->getName());
    }

    /**
     * {@inheritdoc}
     */
    public function fire()
    {
        $package   = $this->getInput()->getArgument('package') ?: null;

        $path      = $this->getInput()->getOption('target-path');
        $override  = (bool)$this->getInput()->getOption('override');
        $force     = (bool)$this->getInput()->getOption('force');

        $defaultFormat = $this->publisher->getFileFormat();

        $this->preparePublisher($force, $defaultFormat);

        $this->publisher->publish($package, $path, $override, $force);

        $this->publisher->setFileFormat($defaultFormat);
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Publish package resources';
    }

    /**
     * {@inheritdoc}
     */
    protected function getArguments()
    {
        return [
            ['package', InputArgument::OPTIONAL, 'select a package by its alias']
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getOptions()
    {
        return [
            ['target-path', null, InputOption::VALUE_OPTIONAL, 'select a different target path.'],
            ['override', false, InputOption::VALUE_NONE, 'Override existing configuration'],
            ['force', false, InputOption::VALUE_NONE, 'Force a default configuration to be published'],
            [
                'format',
                PackagePublisher::FORMAT_XML,
                InputOption::VALUE_OPTIONAL,
                'Config format if --force is selected.'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscriptions()
    {
        if (!$logger = $this->getLogger()) {
            return [];
        }

        return [
            PublishEvents::EVENT_PUBLISH_PACKAGE => 'onPublish',
            PublishEvents::EVENT_PUBLISHED => 'onPublished',
            PublishEvents::EVENT_NOT_PUBLISHED => 'onNotPublished',
            PublishEvents::EVENT_PUBLISH_EXCEPTION => 'onPublishException'
        ];
    }

    /**
     * onPublish
     *
     * @param PackageEvent $event
     *
     * @return void
     */
    public function onPublish(PackageEvent $event)
    {
        $this->getOutput()->writeln(
            sprintf('Publishing package %s', $this->setColor($event->getPackage()->getName(), 'cyan'))
        );
    }

    /**
     * onPublished
     *
     * @param PackagePublishEvent $event
     *
     * @return void
     */
    public function onPublished(PackagePublishEvent $event)
    {
        $this->getLogger()->info(
            sprintf('[%s]: published file "%s".', $event->getPackage()->getName(), $event->getFile())
        );
    }

    /**
     * onNotPublished
     *
     * @param PackagePublishEvent $event
     *
     * @return void
     */
    public function onNotPublished(PackagePublishEvent $event)
    {
        $this->getLogger()->warning(
            sprintf('[%s]: not published file "%s".', $event->getPackage()->getName(), $event->getFile())
        );
    }

    /**
     * onPublishException
     *
     * @param PackageExceptionEvent $event
     *
     * @return void
     */
    public function onPublishException(PackageExceptionEvent $event)
    {
        $this->getLogger()->error(
            sprintf('[%s]: %s.', $event->getPackage()->getName(), $event->getException()->getMessage())
        );
    }

    /**
     * preparePublisher
     *
     * @param boolean $force
     * @param string $defaultFormat
     *
     * @return void
     */
    private function preparePublisher($force, $defaultFormat)
    {
        if (($format = $this->getInput()->getOption('format')) && !$force) {
            throw new \InvalidArgumentException('Option --extension only allowed if option --force is set.');
        }

        $this->publisher->setFileFormat($format ?: $defaultFormat);

        if ($events = $this->getEvents()) {
            $this->publisher->setEvents($events);
        }
    }
}
