<?php

/**
 * This File is part of the Selene\Packages\Framework\View\Events package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\View\Events;

/**
 * @class ComposeEvent
 * @package Selene\Packages\Framework\View\Events
 * @version $Id$
 */
class ComposeEvent extends RenderEvent
{
}
