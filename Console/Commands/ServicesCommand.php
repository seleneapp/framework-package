<?php

/**
 * This File is part of the Selene\Packages\Framework\Console\Commands package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Console\Commands;

use \Symfony\Component\Console\Application;
use \Symfony\Component\Console\Command\Command;
use \Symfony\Component\Console\Input\ArrayInput;
use \Symfony\Component\Console\Output\NullOutput;
use \Symfony\Component\Console\Input\InputOption;
use \Symfony\Component\Console\Output\OutputInterface;
use \Symfony\Component\Console\Input\InputInterface;

/**
 * @class ServicesCommand
 * @package Selene\Packages\Framework\Console\Commands
 * @version $Id$
 */
class ServicesCommand extends Command
{
    protected function configure()
    {
        $this->setName('foo:services');
        $this->setDescription('List all application routes');
    }

    /**
     * execute
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @access protected
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $app = $this->getApplication()->getApplication();
        $container = $app->getContainer();

        $output->writeln('container has services');
    }
}
