<?php

/**
* This File is part of the Selene\Packages\Framework\View\Events package
*
* (c) Thomas Appel <mail@thomas-appel.com>
*
* For full copyright and license information, please refer to the LICENSE file
* that was distributed with this package.
*/

namespace Selene\Packages\Framework\View\Events;

use \Selene\Components\Events\Event;
use \Selene\Components\View\Template\EngineInterface;

/**
* @class RenderEvent
* @package Selene\Packages\Framework\View\Events
* @version $Id$
*/
class ResolveEvent extends Event
{
    private $engine;

    public function __construct(EngineInterface $engine)
    {
        $this->engine = $engine;
    }

    /**
     * setTemplate
     *
     * @param mixed $template
     *
     * @return void
     */
    public function setEngine(EngineInterface $engine)
    {
        $this->engine = $engine;
    }

    /**
     * getTemplate
     *
     * @return mixed
     */
    public function getEngine()
    {
        return $this->engine;
    }
}
