<?php

/**
 * This File is part of the Selene\Packages\Framework\View\Events package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\View\Events;

use \Selene\Components\Events\Event;

/**
 * @class RenderEvent
 * @package Selene\Packages\Framework\View\Events
 * @version $Id$
 */
class RenderEvent extends Event
{
    private $template;

    public function __construct($template = null)
    {
        $this->template = $template;
    }

    /**
     * setTemplate
     *
     * @param mixed $template
     *
     * @return void
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * getTemplate
     *
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }
}
