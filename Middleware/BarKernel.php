<?php

/**
 * This File is part of the Selene\Packages\Framework\Middleware package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Middleware;

use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;

/**
 * @class BarKernel extends StubKernel
 * @see StubKernel
 *
 * @package Selene\Packages\Framework\Middleware
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class BarKernel extends StubKernel
{
    protected $priority = 30;

    public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = true)
    {
        var_dump('browsers!');
        return $this->getKernel()->handle($request, $type, $catch);
    }
}
