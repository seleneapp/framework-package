<?php

/**
 * This File is part of the Selene\Packages\Framework\Process package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Packages\Framework\Process;

use \Selene\Components\DI\Reference;
use \Selene\Components\DI\ContainerInterface;
use \Selene\Components\DI\Processor\ProcessInterface;
use \Selene\Components\DI\Definition\ServiceDefinition;
use \Selene\Components\DI\Definition\DefinitionInterface;

/**
 * @class ReplaceViewManger
 * @package Selene\Packages\Framework\Process
 * @version $Id$
 */
class ReplaceViewManger implements ProcessInterface
{
    private $container;

    public function process(ContainerInterface $container)
    {
        $this->container = $container;

        if (!$container->hasDefinition('view')) {
            return;
        }

        if (!$container->getParameters()->has('view.events') || !$container->getParameters()->get('view.events')) {
            return;
        }

        $this->replaceViewManagerReference($container->getDefinition('view'));
    }

    protected function replaceViewManagerReference(DefinitionInterface $definition)
    {
        $args = $definition->getArguments();

        array_unshift($args, new Reference('app.events'));

        var_dump($args);

        $definition->setClass($this->getManagerClass());
        $definition->setArguments($args);
    }

    protected function getManagerClass()
    {
        return 'Selene\Packages\Framework\View\EventedView';
    }
}
